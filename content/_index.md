## Systemische Therapie und Beratung

Systemische Beratung ist ein Oberbegriff für verschiedene Beratungsformate. Dieser Oberbegriff
bezeichnet die Beratung von Individuen oder Gruppen in Bezug auf deren jeweiligen Kontext: ihr soziales System.

Systemische Beratung bezieht sich auf die Grundlagen der Systemtheorie, der systemischen Familientherapie und der Beratungswissenschaft. Sie erklärt das Verhalten von Menschen nicht isoliert aus deren inneren Eigenschaften heraus, sondern aus ihren Beziehungen und Interaktionen untereinander und zu ihrer Systemumwelt.
